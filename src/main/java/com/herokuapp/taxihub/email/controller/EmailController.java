package com.herokuapp.taxihub.email.controller;

import com.herokuapp.taxihub.email.body.Email;
import com.herokuapp.taxihub.email.service.EmailSenderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmailController {

    private final EmailSenderService emailSenderService;

    public EmailController(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @PostMapping("/send")
    public void sendEmail(@RequestBody Email email) {
        emailSenderService.sendMail(email);
    }
}
