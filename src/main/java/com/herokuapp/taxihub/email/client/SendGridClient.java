package com.herokuapp.taxihub.email.client;

import com.sendgrid.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SendGridClient implements EmailClient {

    @Autowired
    private SendGrid sendGrid;

    @Override
    public void send(com.herokuapp.taxihub.email.body.Email email) {
        System.out.println("TEST: SendGridClient");
        Email from = new Email("javawro6@gmail.com");
        Mail mail = new Mail(from, email.getSubject(), new Email(email.getTo()), new Content("text", email.getContent()));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sendGrid.api(request);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
