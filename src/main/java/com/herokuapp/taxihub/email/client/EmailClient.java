package com.herokuapp.taxihub.email.client;

import com.herokuapp.taxihub.email.body.Email;

public interface EmailClient {
    void send(Email email);
}
