package com.herokuapp.taxihub.email.client;

import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Email;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class MailJetClient implements EmailClient {

    @Autowired
    private Environment env;

    @Override
    public void send(com.herokuapp.taxihub.email.body.Email email) {
        String apiKey = env.getProperty("apiKey");
        String apiSecret = env.getProperty("apiSecret");
        String sender = env.getProperty("sender");

        System.out.println("TEST: MailJetClient");
        System.out.println(apiKey);
        System.out.println(sender);
        System.out.println(apiSecret);
        System.out.println(email);

        MailjetClient client = new MailjetClient(apiKey, apiSecret);

        MailjetRequest mail =
                new MailjetRequest(Email.resource)
                        .property(Email.FROMEMAIL, sender)
                        .property(Email.SUBJECT, email.getSubject())
                        .property(Email.TEXTPART, email.getContent())
                        .property(Email.HTMLPART, email.getContent())
                        .property(Email.RECIPIENTS, new JSONArray().put(new JSONObject().put("Email", email.getTo())));
        try {
            client.post(mail);
        } catch (MailjetException | MailjetSocketTimeoutException e) {
            e.printStackTrace();
        }
    }
}


