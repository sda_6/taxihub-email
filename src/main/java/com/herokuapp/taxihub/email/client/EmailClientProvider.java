package com.herokuapp.taxihub.email.client;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;


@Service
public class EmailClientProvider {

    private ApplicationContext applicationContext;

    public EmailClientProvider(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public EmailClient get(String provider) {
        if ("sendgrid".equals(provider))
            return applicationContext.getBean(SendGridClient.class);

        return applicationContext.getBean(MailJetClient.class);
    }
}
