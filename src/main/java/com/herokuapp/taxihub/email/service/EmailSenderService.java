package com.herokuapp.taxihub.email.service;

import com.herokuapp.taxihub.email.body.Email;
import com.herokuapp.taxihub.email.client.EmailClientProvider;
import org.springframework.stereotype.Service;


@Service
public class EmailSenderService {

    private final EmailClientProvider provider;

    public EmailSenderService(EmailClientProvider provider) {
        this.provider = provider;
    }

    public void sendMail(Email email) {
        provider.get(email.getProvider()).send(email);
    }

}

